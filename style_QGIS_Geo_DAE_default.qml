<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" minScale="100000000" labelsEnabled="0" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" version="3.16.9-Hannover" simplifyMaxScale="1" simplifyDrawingHints="0" readOnly="0" maxScale="0" simplifyDrawingTol="1" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" startField="" fixedDuration="0" durationUnit="min" enabled="0" durationField="" endExpression="" startExpression="" endField="" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" enableorderby="0" forceraster="0" type="singleSymbol">
    <symbols>
      <symbol alpha="1" type="marker" force_rhr="0" name="0" clip_to_extent="1">
        <layer pass="0" locked="0" enabled="1" class="SimpleMarker">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0.00000000000000006,2.39999999999999947" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer pass="0" locked="0" enabled="1" class="SvgMarker">
          <prop v="0" k="angle"/>
          <prop v="229,182,54,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjE1cHgiCiAgIGhlaWdodD0iMTVweCIKICAgdmlld0JveD0iMCAwIDE1IDE1IgogICBpZD0iZGVmaWJyaWxsYXRvciIKICAgdmVyc2lvbj0iMS4xIgogICBzb2RpcG9kaTpkb2NuYW1lPSJkZWZpYnJpbGxhdG9yLXN2Z3JlcG8tY29tLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi41ICgyMDYwZWMxZjlmLCAyMDIwLTA0LTA4KSI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhOSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczciIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxNjAwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjgxNCIKICAgICBpZD0ibmFtZWR2aWV3NSIKICAgICBzaG93Z3JpZD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6em9vbT0iMTUuNzMzMzMzMzMzMyIKICAgICBpbmtzY2FwZTpjeD0iLTAuNDQ0OTE1MjU0MjM3IgogICAgIGlua3NjYXBlOmN5PSI3LjUiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjAiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9IjI1IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZGVmaWJyaWxsYXRvciIgLz4KICA8cGF0aAogICAgIGQ9Ik0xLjU1LDYuMzM4MUMtMC44MzY4LDEuNzQxNiw1LjE4LTEuMzIyOCw3LjUwMiwzLjI3MzdjMi4zMjE1LTQuNTk2NSw4LjMzODctMS41MzIyLDUuOTUyMywzLjA2NDQtMC4wODY5LjE2NzEtLjIwMjgsMC4zNDU2LTAuMzExNCwwLjUyMTJIMTEuMzM1TDkuNTIwNSw0LjEzNzdBMC42MjUyLDAuNjI1MiwwLDAsMCw4LjQyLDQuMjUyNGwtMS42NDg0LDQuMTItMS4zMy0xLjMzQTAuNjI1NCwwLjYyNTQsMCwwLDAsNSw2Ljg1OTRIMS44NjExQzEuNzUyNSw2LjY4MzcsMS42MzY2LDYuNTA1MiwxLjU1LDYuMzM4MVpNMTEsOC4xMDk0YTAuNjI2MywwLjYyNjMsMCwwLDEtLjUyMDUtMC4yNzgzTDkuMTM4Nyw1LjgyLDcuNTgsOS43MTY4YTAuNjI0NSwwLjYyNDUsMCwwLDEtLjQ1NDYuMzhBMC42MDQ3LDAuNjA0NywwLDAsMSw3LDEwLjEwOTRhMC42MjUxLDAuNjI1MSwwLDAsMS0uNDQxOS0wLjE4MjZMNC43NDEyLDguMTA5NEgyLjczNThhNDIuNjcsNDIuNjcsMCwwLDAsNC40Niw0LjY3MzIsMC40NjQyLDAuNDY0MiwwLDAsMCwuNjIyMiwwLDQzLjI2LDQzLjI2LDAsMCwwLDQuNDUwNS00LjY3MzJIMTFaIgogICAgIGlkPSJwYXRoMiIKICAgICBzdHlsZT0iZmlsbDojMDA4MDAwIiAvPgo8L3N2Zz4K" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="6" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>"nom"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory opacity="1" lineSizeType="MM" sizeType="MM" rotationOffset="270" width="15" backgroundColor="#ffffff" scaleBasedVisibility="0" scaleDependency="Area" enabled="0" labelPlacementMethod="XHeight" showAxis="1" minScaleDenominator="0" spacingUnitScale="3x:0,0,0,0,0,0" penAlpha="255" sizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" diagramOrientation="Up" penColor="#000000" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" height="15" direction="0" barWidth="5" spacingUnit="MM" penWidth="0" maxScaleDenominator="1e+08" spacing="5">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol alpha="1" type="line" force_rhr="0" name="" clip_to_extent="1">
          <layer pass="0" locked="0" enabled="1" class="SimpleLine">
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" placement="0" linePlacementFlags="18" zIndex="0" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat_valid">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="validées" name="validées"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="en attente de validation" name="en attente de validation"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="mises en doute" name="mises en doute"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="x_coor2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="y_coor2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lat_coor1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="long_coor1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="xy_precis">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_adr">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="adr_num">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="adr_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="com_cp">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="com_insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="com_nom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="intérieur" name="intérieur"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="extérieur" name="extérieur"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc_lib">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="oui" name="CheckedState"/>
            <Option type="int" value="1" name="TextDisplayMethod"/>
            <Option type="QString" value="non" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc_pcsec">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="oui" name="CheckedState"/>
            <Option type="int" value="1" name="TextDisplayMethod"/>
            <Option type="QString" value="non" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc_acc">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="oui" name="CheckedState"/>
            <Option type="int" value="1" name="TextDisplayMethod"/>
            <Option type="QString" value="non" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc_etg">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="acc_complt">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="photo2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disp_j">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="&quot;valeur&quot;" name="Description"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="valeur" name="Key"/>
            <Option type="QString" value="liste_disp_j_4616a01a_454e_4a46_a405_10f23c030d5d" name="Layer"/>
            <Option type="QString" value="liste_disp_j" name="LayerName"/>
            <Option type="QString" value="ogr" name="LayerProviderName"/>
            <Option type="QString" value="/home/pasqal/pasqcloud/GITHUB/GEO_DAE/GEO_DAE_QGIS.gpkg|layername=liste_disp_j" name="LayerSource"/>
            <Option type="int" value="3" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="valeur" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disp_h">
      <editWidget type="ValueRelation">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowMulti"/>
            <Option type="bool" value="false" name="AllowNull"/>
            <Option type="QString" value="&quot;valeur&quot;" name="Description"/>
            <Option type="QString" value="" name="FilterExpression"/>
            <Option type="QString" value="valeur" name="Key"/>
            <Option type="QString" value="liste_disp_h_e208755b_fae1_49e8_97c0_837df05729e8" name="Layer"/>
            <Option type="QString" value="liste_disp_h" name="LayerName"/>
            <Option type="QString" value="ogr" name="LayerProviderName"/>
            <Option type="QString" value="/home/pasqal/pasqcloud/GITHUB/GEO_DAE/GEO_DAE_QGIS.gpkg|layername=liste_disp_h" name="LayerSource"/>
            <Option type="int" value="1" name="NofColumns"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="UseCompleter"/>
            <Option type="QString" value="valeur" name="Value"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disp_complt">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tel1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tel2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="site_email">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_instal">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="yyyy-MM-dd" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="etat_fonct">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option type="QString" value="En fonctionnement" name="En fonctionnement"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="Hors service" name="Hors service"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="Supprimé définitivement" name="Supprimé définitivement"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="Absent momentanément" name="Absent momentanément"/>
              </Option>
              <Option type="Map">
                <Option type="QString" value="Inconnu" name="Inconnu"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fab_siren">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fab_rais">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mnt_siren">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mnt_rais">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="modele">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="num_serie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="id_euro">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lc_ped">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="oui" name="CheckedState"/>
            <Option type="int" value="1" name="TextDisplayMethod"/>
            <Option type="QString" value="non" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dtpr_lcped">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="yyyy-MM-dd" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dtpr_lcad">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="yyyy-MM-dd" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dtpr_bat">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="yyyy-MM-dd" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="freq_mnt">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dispsurv">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="oui" name="CheckedState"/>
            <Option type="int" value="1" name="TextDisplayMethod"/>
            <Option type="QString" value="non" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="dermnt">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="allow_null"/>
            <Option type="bool" value="true" name="calendar_popup"/>
            <Option type="QString" value="yyyy-MM-dd" name="display_format"/>
            <Option type="QString" value="yyyy-MM-dd" name="field_format"/>
            <Option type="bool" value="false" name="field_iso_format"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maj_don">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_siren">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_type">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_rais">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_num">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_voie">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_com">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_cp">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_insee">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_tel1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_tel2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="expt_mail">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="gid">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="etat_valid" index="1" name="Validation par l'exploitant des&#xa;données"/>
    <alias field="nom" index="2" name=" Nom donné au DAE"/>
    <alias field="x_coor2" index="3" name="Coordonnée X"/>
    <alias field="y_coor2" index="4" name="Coordonnée Y"/>
    <alias field="lat_coor1" index="5" name="Coordonnée de latitude"/>
    <alias field="long_coor1" index="6" name="Coordonnée de longitude"/>
    <alias field="xy_precis" index="7" name="Précision des coordonnées X et Y"/>
    <alias field="id_adr" index="8" name=""/>
    <alias field="adr_num" index="9" name="Numéro de la voie"/>
    <alias field="adr_voie" index="10" name="Type et nom de la voie ou lieu-dit"/>
    <alias field="com_cp" index="11" name="Code postal de la commune"/>
    <alias field="com_insee" index="12" name="Code Insee de la commune"/>
    <alias field="com_nom" index="13" name="Nom de la commune"/>
    <alias field="acc" index="14" name="Environnement d'accès"/>
    <alias field="acc_lib" index="15" name="Accès libre"/>
    <alias field="acc_pcsec" index="16" name="Présence d'un poste de sécurité"/>
    <alias field="acc_acc" index="17" name="Présence d'un accueil public"/>
    <alias field="acc_etg" index="18" name="Etage d'accessibilité"/>
    <alias field="acc_complt" index="19" name="Complément d'information sur&#xa;l'accès"/>
    <alias field="photo1" index="20" name="url Photo 1 du DAE dans son&#xa;environnement"/>
    <alias field="photo2" index="21" name="url Photo 2 du DAE dans son&#xa;environnement"/>
    <alias field="disp_j" index="22" name="Jours d'accessibilité"/>
    <alias field="disp_h" index="23" name="heures de disponibilité"/>
    <alias field="disp_complt" index="24" name="Complément d'information sur la&#xa;disponibilité"/>
    <alias field="tel1" index="25" name="Numéro 1 de téléphone sur le site "/>
    <alias field="tel2" index="26" name="Numéro 2 de téléphone sur le site"/>
    <alias field="site_email" index="27" name="Adresse email de contact du site"/>
    <alias field="date_instal" index="28" name="Date d'installation"/>
    <alias field="etat_fonct" index="29" name="Etat de fonctionnement"/>
    <alias field="fab_siren" index="30" name="Numéro SIREN du fabricant"/>
    <alias field="fab_rais" index="31" name="Raison sociale du fabricant"/>
    <alias field="mnt_siren" index="32" name="Numéro SIREN du mainteneur"/>
    <alias field="mnt_rais" index="33" name=" Raison sociale du mainteneur"/>
    <alias field="modele" index="34" name="Nom du modèle de DAE"/>
    <alias field="num_serie" index="35" name="Numéro de série du DAE"/>
    <alias field="id_euro" index="36" name="Identifiant unique du dispositif (IUD&#xa;européen)"/>
    <alias field="lc_ped" index="37" name=" Présence d'électrodes pédiatriques"/>
    <alias field="dtpr_lcped" index="38" name="Date de péremption des électrodes&#xa;pédiatriques"/>
    <alias field="dtpr_lcad" index="39" name="Date de péremption des électrodes&#xa;adultes"/>
    <alias field="dtpr_bat" index="40" name="Date de péremption de la batterie"/>
    <alias field="freq_mnt" index="41" name="Fréquence de la maintenance"/>
    <alias field="dispsurv" index="42" name="présence d'un dispositif de surveillance à distance"/>
    <alias field="dermnt" index="43" name="Date de la dernière maintenance"/>
    <alias field="maj_don" index="44" name="Date de la dernière mise à jour des&#xa;données"/>
    <alias field="expt_siren" index="45" name="Numéro SIREN de l'exploitant"/>
    <alias field="expt_type" index="46" name="Code APE de l'exploitant"/>
    <alias field="expt_rais" index="47" name="Raison sociale de l'exploitant,&#xa;personne morale "/>
    <alias field="expt_num" index="48" name="Numéro de la voie où est situé&#xa;l'exploitant"/>
    <alias field="expt_voie" index="49" name="Type et nom de la voie ou lieu-dit&#xa;où est situé l'exploitant"/>
    <alias field="expt_com" index="50" name="Commune où est situé l'exploitant"/>
    <alias field="expt_cp" index="51" name="Code postal où est situé l'exploitant"/>
    <alias field="expt_insee" index="52" name="Code Insee de la commune où est&#xa;situé l'exploitant"/>
    <alias field="expt_tel1" index="53" name="Numéro de téléphone 1 de&#xa;l'exploitant"/>
    <alias field="expt_tel2" index="54" name="Numéro de téléphone 2 de&#xa;l'exploitant "/>
    <alias field="expt_mail" index="55" name="Adresse électronique de&#xa;l'exploitant"/>
    <alias field="gid" index="56" name=""/>
  </aliases>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id"/>
    <default expression="'en attente de validation'" applyOnUpdate="0" field="etat_valid"/>
    <default expression="" applyOnUpdate="0" field="nom"/>
    <default expression="x(transform($geometry,'EPSG:2154','EPSG:4326'))" applyOnUpdate="1" field="x_coor2"/>
    <default expression="y(transform($geometry,'EPSG:2154','EPSG:4326'))" applyOnUpdate="1" field="y_coor2"/>
    <default expression="$y" applyOnUpdate="1" field="lat_coor1"/>
    <default expression="$x" applyOnUpdate="1" field="long_coor1"/>
    <default expression="" applyOnUpdate="0" field="xy_precis"/>
    <default expression="" applyOnUpdate="0" field="id_adr"/>
    <default expression=" get_address( 'num')" applyOnUpdate="0" field="adr_num"/>
    <default expression=" get_address( 'rue')" applyOnUpdate="0" field="adr_voie"/>
    <default expression=" get_address('cop')" applyOnUpdate="0" field="com_cp"/>
    <default expression="array_to_string(overlay_within( 'commune',insee_com,limit:=1))" applyOnUpdate="1" field="com_insee"/>
    <default expression=" get_address( 'VIL')" applyOnUpdate="0" field="com_nom"/>
    <default expression="" applyOnUpdate="0" field="acc"/>
    <default expression="" applyOnUpdate="0" field="acc_lib"/>
    <default expression="" applyOnUpdate="0" field="acc_pcsec"/>
    <default expression="" applyOnUpdate="0" field="acc_acc"/>
    <default expression="" applyOnUpdate="0" field="acc_etg"/>
    <default expression="" applyOnUpdate="0" field="acc_complt"/>
    <default expression="" applyOnUpdate="0" field="photo1"/>
    <default expression="" applyOnUpdate="0" field="photo2"/>
    <default expression="'7j/7'" applyOnUpdate="0" field="disp_j"/>
    <default expression="" applyOnUpdate="0" field="disp_h"/>
    <default expression="" applyOnUpdate="0" field="disp_complt"/>
    <default expression="" applyOnUpdate="0" field="tel1"/>
    <default expression="" applyOnUpdate="0" field="tel2"/>
    <default expression="" applyOnUpdate="0" field="site_email"/>
    <default expression="" applyOnUpdate="0" field="date_instal"/>
    <default expression="'En fonctionnement'" applyOnUpdate="0" field="etat_fonct"/>
    <default expression="" applyOnUpdate="0" field="fab_siren"/>
    <default expression="" applyOnUpdate="0" field="fab_rais"/>
    <default expression="" applyOnUpdate="0" field="mnt_siren"/>
    <default expression="" applyOnUpdate="0" field="mnt_rais"/>
    <default expression="" applyOnUpdate="0" field="modele"/>
    <default expression="" applyOnUpdate="0" field="num_serie"/>
    <default expression="" applyOnUpdate="0" field="id_euro"/>
    <default expression="" applyOnUpdate="0" field="lc_ped"/>
    <default expression="" applyOnUpdate="0" field="dtpr_lcped"/>
    <default expression="" applyOnUpdate="0" field="dtpr_lcad"/>
    <default expression="" applyOnUpdate="0" field="dtpr_bat"/>
    <default expression="" applyOnUpdate="0" field="freq_mnt"/>
    <default expression="" applyOnUpdate="0" field="dispsurv"/>
    <default expression="" applyOnUpdate="0" field="dermnt"/>
    <default expression="" applyOnUpdate="0" field="maj_don"/>
    <default expression="" applyOnUpdate="0" field="expt_siren"/>
    <default expression="" applyOnUpdate="0" field="expt_type"/>
    <default expression="" applyOnUpdate="0" field="expt_rais"/>
    <default expression="" applyOnUpdate="0" field="expt_num"/>
    <default expression="" applyOnUpdate="0" field="expt_voie"/>
    <default expression="" applyOnUpdate="0" field="expt_com"/>
    <default expression="" applyOnUpdate="0" field="expt_cp"/>
    <default expression="" applyOnUpdate="0" field="expt_insee"/>
    <default expression="" applyOnUpdate="0" field="expt_tel1"/>
    <default expression="" applyOnUpdate="0" field="expt_tel2"/>
    <default expression="" applyOnUpdate="0" field="expt_mail"/>
    <default expression="" applyOnUpdate="0" field="gid"/>
  </defaults>
  <constraints>
    <constraint constraints="3" exp_strength="0" unique_strength="1" field="id" notnull_strength="1"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="etat_valid" notnull_strength="2"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="nom" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="x_coor2" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="y_coor2" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="lat_coor1" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="long_coor1" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="xy_precis" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="id_adr" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="adr_num" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="adr_voie" notnull_strength="0"/>
    <constraint constraints="4" exp_strength="2" unique_strength="0" field="com_cp" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="com_insee" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="com_nom" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="acc" notnull_strength="2"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="acc_lib" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="acc_pcsec" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="acc_acc" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="acc_etg" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="acc_complt" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="photo1" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="photo2" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="disp_j" notnull_strength="1"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="disp_h" notnull_strength="2"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="disp_complt" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="tel1" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="tel2" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="site_email" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="date_instal" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="etat_fonct" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="fab_siren" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="fab_rais" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="mnt_siren" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="mnt_rais" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="modele" notnull_strength="1"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="num_serie" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="id_euro" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="lc_ped" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="dtpr_lcped" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="dtpr_lcad" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="dtpr_bat" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="freq_mnt" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="dispsurv" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="dermnt" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="maj_don" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="expt_siren" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_type" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="expt_rais" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_num" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_voie" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_com" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_cp" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_insee" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="expt_tel1" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="expt_tel2" notnull_strength="0"/>
    <constraint constraints="1" exp_strength="0" unique_strength="0" field="expt_mail" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="gid" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="id" desc=""/>
    <constraint exp="" field="etat_valid" desc=""/>
    <constraint exp="" field="nom" desc=""/>
    <constraint exp="" field="x_coor2" desc=""/>
    <constraint exp="" field="y_coor2" desc=""/>
    <constraint exp="" field="lat_coor1" desc=""/>
    <constraint exp="" field="long_coor1" desc=""/>
    <constraint exp="" field="xy_precis" desc=""/>
    <constraint exp="" field="id_adr" desc=""/>
    <constraint exp="" field="adr_num" desc=""/>
    <constraint exp="" field="adr_voie" desc=""/>
    <constraint exp="'^\\d{5}$'" field="com_cp" desc=""/>
    <constraint exp="" field="com_insee" desc=""/>
    <constraint exp="" field="com_nom" desc=""/>
    <constraint exp="" field="acc" desc=""/>
    <constraint exp="" field="acc_lib" desc=""/>
    <constraint exp="" field="acc_pcsec" desc=""/>
    <constraint exp="" field="acc_acc" desc=""/>
    <constraint exp="" field="acc_etg" desc=""/>
    <constraint exp="" field="acc_complt" desc=""/>
    <constraint exp="" field="photo1" desc=""/>
    <constraint exp="" field="photo2" desc=""/>
    <constraint exp="" field="disp_j" desc=""/>
    <constraint exp="" field="disp_h" desc=""/>
    <constraint exp="" field="disp_complt" desc=""/>
    <constraint exp="" field="tel1" desc=""/>
    <constraint exp="" field="tel2" desc=""/>
    <constraint exp="" field="site_email" desc=""/>
    <constraint exp="" field="date_instal" desc=""/>
    <constraint exp="" field="etat_fonct" desc=""/>
    <constraint exp="" field="fab_siren" desc=""/>
    <constraint exp="" field="fab_rais" desc=""/>
    <constraint exp="" field="mnt_siren" desc=""/>
    <constraint exp="" field="mnt_rais" desc=""/>
    <constraint exp="" field="modele" desc=""/>
    <constraint exp="" field="num_serie" desc=""/>
    <constraint exp="" field="id_euro" desc=""/>
    <constraint exp="" field="lc_ped" desc=""/>
    <constraint exp="" field="dtpr_lcped" desc=""/>
    <constraint exp="" field="dtpr_lcad" desc=""/>
    <constraint exp="" field="dtpr_bat" desc=""/>
    <constraint exp="" field="freq_mnt" desc=""/>
    <constraint exp="" field="dispsurv" desc=""/>
    <constraint exp="" field="dermnt" desc=""/>
    <constraint exp="" field="maj_don" desc=""/>
    <constraint exp="" field="expt_siren" desc=""/>
    <constraint exp="" field="expt_type" desc=""/>
    <constraint exp="" field="expt_rais" desc=""/>
    <constraint exp="" field="expt_num" desc=""/>
    <constraint exp="" field="expt_voie" desc=""/>
    <constraint exp="" field="expt_com" desc=""/>
    <constraint exp="" field="expt_cp" desc=""/>
    <constraint exp="" field="expt_insee" desc=""/>
    <constraint exp="" field="expt_tel1" desc=""/>
    <constraint exp="" field="expt_tel2" desc=""/>
    <constraint exp="" field="expt_mail" desc=""/>
    <constraint exp="" field="gid" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column width="-1" type="field" hidden="0" name="id"/>
      <column width="-1" type="field" hidden="0" name="etat_valid"/>
      <column width="-1" type="field" hidden="0" name="nom"/>
      <column width="-1" type="field" hidden="0" name="x_coor2"/>
      <column width="-1" type="field" hidden="0" name="y_coor2"/>
      <column width="-1" type="field" hidden="0" name="lat_coor1"/>
      <column width="-1" type="field" hidden="0" name="long_coor1"/>
      <column width="-1" type="field" hidden="0" name="xy_precis"/>
      <column width="-1" type="field" hidden="0" name="id_adr"/>
      <column width="-1" type="field" hidden="0" name="adr_num"/>
      <column width="-1" type="field" hidden="0" name="adr_voie"/>
      <column width="-1" type="field" hidden="0" name="com_cp"/>
      <column width="221" type="field" hidden="0" name="com_insee"/>
      <column width="-1" type="field" hidden="0" name="com_nom"/>
      <column width="-1" type="field" hidden="0" name="acc"/>
      <column width="-1" type="field" hidden="0" name="acc_lib"/>
      <column width="-1" type="field" hidden="0" name="acc_pcsec"/>
      <column width="-1" type="field" hidden="0" name="acc_acc"/>
      <column width="-1" type="field" hidden="0" name="acc_etg"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" hidden="0" name="acc_complt"/>
      <column width="-1" type="field" hidden="0" name="photo1"/>
      <column width="-1" type="field" hidden="0" name="photo2"/>
      <column width="-1" type="field" hidden="0" name="disp_j"/>
      <column width="-1" type="field" hidden="0" name="disp_h"/>
      <column width="-1" type="field" hidden="0" name="disp_complt"/>
      <column width="-1" type="field" hidden="0" name="tel1"/>
      <column width="-1" type="field" hidden="0" name="tel2"/>
      <column width="-1" type="field" hidden="0" name="site_email"/>
      <column width="-1" type="field" hidden="0" name="date_instal"/>
      <column width="-1" type="field" hidden="0" name="etat_fonct"/>
      <column width="-1" type="field" hidden="0" name="fab_siren"/>
      <column width="-1" type="field" hidden="0" name="fab_rais"/>
      <column width="-1" type="field" hidden="0" name="mnt_siren"/>
      <column width="-1" type="field" hidden="0" name="mnt_rais"/>
      <column width="-1" type="field" hidden="0" name="modele"/>
      <column width="-1" type="field" hidden="0" name="num_serie"/>
      <column width="-1" type="field" hidden="0" name="id_euro"/>
      <column width="-1" type="field" hidden="0" name="lc_ped"/>
      <column width="-1" type="field" hidden="0" name="dtpr_lcped"/>
      <column width="-1" type="field" hidden="0" name="dtpr_lcad"/>
      <column width="-1" type="field" hidden="0" name="dtpr_bat"/>
      <column width="-1" type="field" hidden="0" name="freq_mnt"/>
      <column width="-1" type="field" hidden="0" name="dispsurv"/>
      <column width="-1" type="field" hidden="0" name="dermnt"/>
      <column width="-1" type="field" hidden="0" name="maj_don"/>
      <column width="-1" type="field" hidden="0" name="expt_siren"/>
      <column width="-1" type="field" hidden="0" name="expt_type"/>
      <column width="-1" type="field" hidden="0" name="expt_rais"/>
      <column width="-1" type="field" hidden="0" name="expt_num"/>
      <column width="-1" type="field" hidden="0" name="expt_voie"/>
      <column width="-1" type="field" hidden="0" name="expt_com"/>
      <column width="-1" type="field" hidden="0" name="expt_cp"/>
      <column width="-1" type="field" hidden="0" name="expt_insee"/>
      <column width="-1" type="field" hidden="0" name="expt_tel1"/>
      <column width="-1" type="field" hidden="0" name="expt_tel2"/>
      <column width="-1" type="field" hidden="0" name="expt_mail"/>
      <column width="-1" type="field" hidden="0" name="gid"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField showLabel="1" index="1" name="etat_valid"/>
    <attributeEditorField showLabel="1" index="2" name="nom"/>
    <attributeEditorField showLabel="1" index="29" name="etat_fonct"/>
    <attributeEditorContainer visibilityExpressionEnabled="0" showLabel="1" visibilityExpression="" columnCount="1" groupBox="0" name="localisation DAE">
      <attributeEditorField showLabel="1" index="3" name="x_coor2"/>
      <attributeEditorField showLabel="1" index="4" name="y_coor2"/>
      <attributeEditorField showLabel="1" index="5" name="lat_coor1"/>
      <attributeEditorField showLabel="1" index="6" name="long_coor1"/>
      <attributeEditorField showLabel="1" index="7" name="xy_precis"/>
      <attributeEditorField showLabel="1" index="9" name="adr_num"/>
      <attributeEditorField showLabel="1" index="10" name="adr_voie"/>
      <attributeEditorField showLabel="1" index="11" name="com_cp"/>
      <attributeEditorField showLabel="1" index="12" name="com_insee"/>
      <attributeEditorField showLabel="1" index="13" name="com_nom"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="0" showLabel="1" visibilityExpression="" columnCount="1" groupBox="0" name="Accès DAE">
      <attributeEditorField showLabel="1" index="14" name="acc"/>
      <attributeEditorField showLabel="1" index="15" name="acc_lib"/>
      <attributeEditorField showLabel="1" index="16" name="acc_pcsec"/>
      <attributeEditorField showLabel="1" index="17" name="acc_acc"/>
      <attributeEditorField showLabel="1" index="18" name="acc_etg"/>
      <attributeEditorField showLabel="1" index="19" name="acc_complt"/>
      <attributeEditorField showLabel="1" index="20" name="photo1"/>
      <attributeEditorField showLabel="1" index="21" name="photo2"/>
      <attributeEditorField showLabel="1" index="22" name="disp_j"/>
      <attributeEditorField showLabel="1" index="23" name="disp_h"/>
      <attributeEditorField showLabel="1" index="24" name="disp_complt"/>
      <attributeEditorField showLabel="1" index="25" name="tel1"/>
      <attributeEditorField showLabel="1" index="26" name="tel2"/>
      <attributeEditorField showLabel="1" index="27" name="site_email"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="0" showLabel="1" visibilityExpression="" columnCount="1" groupBox="0" name="caractéristiques DAE">
      <attributeEditorField showLabel="1" index="28" name="date_instal"/>
      <attributeEditorField showLabel="1" index="30" name="fab_siren"/>
      <attributeEditorField showLabel="1" index="31" name="fab_rais"/>
      <attributeEditorField showLabel="1" index="32" name="mnt_siren"/>
      <attributeEditorField showLabel="1" index="33" name="mnt_rais"/>
      <attributeEditorField showLabel="1" index="34" name="modele"/>
      <attributeEditorField showLabel="1" index="35" name="num_serie"/>
      <attributeEditorField showLabel="1" index="37" name="lc_ped"/>
      <attributeEditorField showLabel="1" index="38" name="dtpr_lcped"/>
      <attributeEditorField showLabel="1" index="39" name="dtpr_lcad"/>
      <attributeEditorField showLabel="1" index="41" name="freq_mnt"/>
      <attributeEditorField showLabel="1" index="42" name="dispsurv"/>
      <attributeEditorField showLabel="1" index="43" name="dermnt"/>
      <attributeEditorField showLabel="1" index="40" name="dtpr_bat"/>
      <attributeEditorField showLabel="1" index="36" name="id_euro"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="0" showLabel="1" visibilityExpression="" columnCount="1" groupBox="0" name="exploitant DAE">
      <attributeEditorField showLabel="1" index="45" name="expt_siren"/>
      <attributeEditorField showLabel="1" index="46" name="expt_type"/>
      <attributeEditorField showLabel="1" index="47" name="expt_rais"/>
      <attributeEditorField showLabel="1" index="48" name="expt_num"/>
      <attributeEditorField showLabel="1" index="49" name="expt_voie"/>
      <attributeEditorField showLabel="1" index="51" name="expt_cp"/>
      <attributeEditorField showLabel="1" index="50" name="expt_com"/>
      <attributeEditorField showLabel="1" index="52" name="expt_insee"/>
      <attributeEditorField showLabel="1" index="53" name="expt_tel1"/>
      <attributeEditorField showLabel="1" index="54" name="expt_tel2"/>
      <attributeEditorField showLabel="1" index="55" name="expt_mail"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="acc" editable="1"/>
    <field name="acc_acc" editable="1"/>
    <field name="acc_complt" editable="1"/>
    <field name="acc_etg" editable="1"/>
    <field name="acc_lib" editable="1"/>
    <field name="acc_pcsec" editable="1"/>
    <field name="adr_num" editable="1"/>
    <field name="adr_voie" editable="1"/>
    <field name="com_cp" editable="1"/>
    <field name="com_insee" editable="1"/>
    <field name="com_nom" editable="1"/>
    <field name="date_instal" editable="1"/>
    <field name="dermnt" editable="1"/>
    <field name="disp_complt" editable="1"/>
    <field name="disp_h" editable="1"/>
    <field name="disp_j" editable="1"/>
    <field name="dispsurv" editable="1"/>
    <field name="dtpr_bat" editable="1"/>
    <field name="dtpr_lcad" editable="1"/>
    <field name="dtpr_lcped" editable="1"/>
    <field name="etat_fonct" editable="1"/>
    <field name="etat_valid" editable="1"/>
    <field name="expt_com" editable="1"/>
    <field name="expt_cp" editable="1"/>
    <field name="expt_insee" editable="1"/>
    <field name="expt_mail" editable="1"/>
    <field name="expt_num" editable="1"/>
    <field name="expt_rais" editable="1"/>
    <field name="expt_siren" editable="1"/>
    <field name="expt_tel1" editable="1"/>
    <field name="expt_tel2" editable="1"/>
    <field name="expt_type" editable="1"/>
    <field name="expt_voie" editable="1"/>
    <field name="fab_rais" editable="1"/>
    <field name="fab_siren" editable="1"/>
    <field name="freq_mnt" editable="1"/>
    <field name="gid" editable="0"/>
    <field name="id" editable="0"/>
    <field name="id_adr" editable="0"/>
    <field name="id_euro" editable="1"/>
    <field name="lat_coor1" editable="1"/>
    <field name="lc_ped" editable="1"/>
    <field name="long_coor1" editable="1"/>
    <field name="maj_don" editable="0"/>
    <field name="mnt_rais" editable="1"/>
    <field name="mnt_siren" editable="1"/>
    <field name="modele" editable="1"/>
    <field name="nom" editable="1"/>
    <field name="num_serie" editable="1"/>
    <field name="photo1" editable="1"/>
    <field name="photo2" editable="1"/>
    <field name="site_email" editable="1"/>
    <field name="tel1" editable="1"/>
    <field name="tel2" editable="1"/>
    <field name="x_coor2" editable="1"/>
    <field name="xy_precis" editable="1"/>
    <field name="y_coor2" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="acc"/>
    <field labelOnTop="0" name="acc_acc"/>
    <field labelOnTop="0" name="acc_complt"/>
    <field labelOnTop="0" name="acc_etg"/>
    <field labelOnTop="0" name="acc_lib"/>
    <field labelOnTop="0" name="acc_pcsec"/>
    <field labelOnTop="0" name="adr_num"/>
    <field labelOnTop="0" name="adr_voie"/>
    <field labelOnTop="0" name="com_cp"/>
    <field labelOnTop="0" name="com_insee"/>
    <field labelOnTop="0" name="com_nom"/>
    <field labelOnTop="0" name="date_instal"/>
    <field labelOnTop="0" name="dermnt"/>
    <field labelOnTop="0" name="disp_complt"/>
    <field labelOnTop="0" name="disp_h"/>
    <field labelOnTop="0" name="disp_j"/>
    <field labelOnTop="0" name="dispsurv"/>
    <field labelOnTop="0" name="dtpr_bat"/>
    <field labelOnTop="0" name="dtpr_lcad"/>
    <field labelOnTop="0" name="dtpr_lcped"/>
    <field labelOnTop="0" name="etat_fonct"/>
    <field labelOnTop="0" name="etat_valid"/>
    <field labelOnTop="0" name="expt_com"/>
    <field labelOnTop="0" name="expt_cp"/>
    <field labelOnTop="0" name="expt_insee"/>
    <field labelOnTop="0" name="expt_mail"/>
    <field labelOnTop="0" name="expt_num"/>
    <field labelOnTop="0" name="expt_rais"/>
    <field labelOnTop="0" name="expt_siren"/>
    <field labelOnTop="0" name="expt_tel1"/>
    <field labelOnTop="0" name="expt_tel2"/>
    <field labelOnTop="0" name="expt_type"/>
    <field labelOnTop="0" name="expt_voie"/>
    <field labelOnTop="0" name="fab_rais"/>
    <field labelOnTop="0" name="fab_siren"/>
    <field labelOnTop="0" name="freq_mnt"/>
    <field labelOnTop="0" name="gid"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_adr"/>
    <field labelOnTop="0" name="id_euro"/>
    <field labelOnTop="0" name="lat_coor1"/>
    <field labelOnTop="0" name="lc_ped"/>
    <field labelOnTop="0" name="long_coor1"/>
    <field labelOnTop="0" name="maj_don"/>
    <field labelOnTop="0" name="mnt_rais"/>
    <field labelOnTop="0" name="mnt_siren"/>
    <field labelOnTop="0" name="modele"/>
    <field labelOnTop="0" name="nom"/>
    <field labelOnTop="0" name="num_serie"/>
    <field labelOnTop="0" name="photo1"/>
    <field labelOnTop="0" name="photo2"/>
    <field labelOnTop="0" name="site_email"/>
    <field labelOnTop="0" name="tel1"/>
    <field labelOnTop="0" name="tel2"/>
    <field labelOnTop="0" name="x_coor2"/>
    <field labelOnTop="0" name="xy_precis"/>
    <field labelOnTop="0" name="y_coor2"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"nom"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
