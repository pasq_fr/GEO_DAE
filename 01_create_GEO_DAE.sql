-- Drop table si nécessaire
-- DROP TABLE public."GEO_DAE_declaration";

-- exemple de  changement check  TABLE public."GEO_DAE_declaration";
--ALTER DOMAIN c_tel DROP CONSTRAINT c_tel_check;
--ALTER DOMAIN c_tel ADD CONSTRAINT c_tel_check CHECK(VALUE ~ '/^(\+33|\+590|\+594|\+262|\+596|\+269|\+687|\+689|\+508|\+681)(\d){6,9}');

-- creation de domaine pour verif code postal, insee...
CREATE DOMAIN c_5_chif AS int CHECK(VALUE::TEXT ~ '^\d{5}$');
CREATE DOMAIN c_9_chif AS int CHECK(VALUE::TEXT ~ '^\d{9}$');
CREATE DOMAIN c_insee AS TEXT CHECK(VALUE ~ '^([013-9]\d|2[AB1-9])\d{3}$');
CREATE DOMAIN c_url_img AS TEXT CHECK(VALUE ~ '^https?:\/\/(?:[a-z0-9\\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpg|jpeg|gif|png)');
CREATE DOMAIN c_tel AS TEXT CHECK(VALUE ~ '/^(\+33|\+590|\+594|\+262|\+596|\+269|\+687|\+689|\+508|\+681)(\d){6,9}');
CREATE DOMAIN c_mail AS TEXT CHECK(VALUE ~'^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'); -- peut-être retirer

--type enumeration
CREATE TYPE e_etat_valid AS ENUM ('validées', 'en attente de validation', 'mises en doute' );
CREATE TYPE e_acc AS ENUM ('intérieur', 'extérieur');
CREATE TYPE e_ouinon AS ENUM ('oui', 'non');
--CREATE TYPE e_disp_j AS ENUM ('7j/7','lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche','jours fériés','événements');
--CREATE TYPE e_disp_h AS ENUM ('heures ouvrables','heures de nuit','24h/h','non renseigné');
CREATE TYPE e_etat_fonct AS ENUM ('En fonctionnement', 'Hors service','Supprimé définitivement','Absent momentanément','Inconnu');

CREATE TABLE public."GEO_DAE_declaration" (
	id SERIAL NOT NULL, -- identifiant
	gid varchar NULL, --Identifiant interne du DAE dans la base de données
	geom geometry(point, 2154) NOT NULL, -- geométrie
	etat_valid varchar NULL, -- Validation par l'exploitant des données
	nom varchar NOT NULL, -- Nom donné au DAE
	x_coor2 float8 NULL, -- Coordonnée X
	y_coor2 float8 NULL, -- Coordonnée Y
	lat_coor1 float8 NOT NULL, -- Coordonnée de latitude
	long_coor1 float8 NOT NULL, -- Coordonnée de longitude
	xy_precis float8 NULL, -- Précision des coordonnées X et Y
	id_adr varchar NULL, -- Clé d'interopérabilité
	adr_num varchar NULL, -- Numéro de la voie et, le cas échéant, sufixe, d'implantation du DAE
	adr_voie varchar NULL, -- Type et nom de la voie ou lieu-dit d'implantation du DAE
	com_cp c_5_chif NULL, -- Code postal de la commune d'implantation du DAE
	com_insee c_insee NULL, -- Code Insee de la commune d'implantation du DAE
	com_nom varchar NULL, -- Nom de la commune d'implantation du DAE
	acc e_acc NOT NULL, -- Environnement d'accès du DAE
	acc_lib e_ouinon NOT NULL, -- Accès libre du DAE
	acc_pcsec e_ouinon NULL, -- Présence d'un poste de sécurité
	acc_acc e_ouinon NULL, -- Présence d'un accueil public
	acc_etg varchar NULL, -- Etage d'accessibilité du DAE
	acc_complt varchar NULL, -- Complément d'information sur l'accès au DAE
	photo1 c_url_img NULL, -- Photo 1 du DAE dans son environnement
	photo2 c_url_img NULL, -- Photo 2 du DAE dans son environnement
	disp_j _text NOT NULL, -- Jours d'accessibilité de l'appareil
	disp_h _text NOT NULL, -- Heures d'accessibilité de l'appareil
	disp_complt varchar NULL, -- Complément d'information sur la disponibilité du DAE
	tel1 c_tel NOT NULL, -- Numéro de téléphone 1 sur le site d'implantation du DAE
	tel2 c_tel NULL, -- Numéro 2 de téléphone sur le site d'implantation du DAE
	site_email c_mail NULL, -- Adresse email de contact du site où le DAE a été implanté
	date_instal date NULL, -- Date d'installation du DAE
	etat_fonct e_etat_fonct NOT NULL DEFAULT 'En fonctionnement', -- Etat de fonctionnement du DAE
	fab_siren c_9_chif NULL, -- Numéro SIREN du fabricant du DAE
	fab_rais varchar NOT NULL, -- Raison sociale du fabricant du DAE
	mnt_siren c_9_chif NULL, -- Numéro SIREN du mainteneur du DAE
	mnt_rais varchar NULL, -- Raison sociale du mainteneur
	modele varchar NOT NULL, -- Nom du modèle de DAE
	num_serie varchar NOT NULL, -- Numéro de série du DAE
	id_euro varchar NULL, -- Identifiant unique du dispositif (IUD européen)
	lc_ped e_ouinon NULL, -- Présence d'électrodes pédiatriques
	dtpr_lcped date NULL, -- Date de péremption des électrodes pédiatriques
	dtpr_lcad date NULL, -- Date de péremption des électrodes adultes
	dtpr_bat date NULL, -- Date de péremption de la batterie
	freq_mnt varchar NULL, -- Fréquence de la maintenance
	dispsurv e_ouinon NULL, -- Dispositif de surveillance à distance du DAE
	dermnt date NOT NULL, -- Date de la dernière maintenance du DAE
	maj_don varchar NULL, -- Date de la dernière mise à jour des données
	expt_siren c_9_chif NULL, -- Numéro SIREN de l'exploitant
	expt_type varchar NULL, -- Code APE de l'exploitant
	expt_rais varchar NULL, -- Raison sociale de l'exploitant - personne morale
	expt_num varchar NULL, -- Numéro de la voie et, le cas échéant, sufixe, où est situé l'exploitant
	expt_voie varchar NULL, -- Type et nom de la voie ou lieu-dit où est situé l'exploitant
	expt_com varchar NULL, -- Commune où est situé l'exploitant
	expt_cp c_5_chif NULL, -- Code postal où est situé l'exploitant
	expt_insee c_insee NULL, -- Code Insee de la commune où est situé l'exploitant
	expt_tel1 c_tel NULL, -- Numéro de téléphone 1 de l'exploitant
	expt_tel2 c_tel NULL, -- Numéro de téléphone 2 de l'exploitant
	expt_mail c_mail NULL, -- Adresse électronique de l'exploitant
	CONSTRAINT "GEO_DAE_declaration_pkey" PRIMARY KEY (id)
);
CREATE INDEX "sidx_GEO_DAE_declaration_geom" ON public."GEO_DAE_declaration" USING gist (geom);

-- Column comments

COMMENT ON COLUMN public."GEO_DAE_declaration".id IS 'identifiant propre à cette base';
COMMENT ON COLUMN public."GEO_DAE_declaration".gid IS 'Identifiant interne du DAE dans la base de données';
COMMENT ON COLUMN public."GEO_DAE_declaration".geom IS 'geométrie';
COMMENT ON COLUMN public."GEO_DAE_declaration".etat_valid IS 'Validation par l''exploitant des données';
COMMENT ON COLUMN public."GEO_DAE_declaration".nom IS 'Nom donné au DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".x_coor2 IS 'Coordonnée X';
COMMENT ON COLUMN public."GEO_DAE_declaration".y_coor2 IS 'Coordonnée Y';
COMMENT ON COLUMN public."GEO_DAE_declaration".lat_coor1 IS 'Coordonnée de latitude';
COMMENT ON COLUMN public."GEO_DAE_declaration".long_coor1 IS 'Coordonnée de longitude';
COMMENT ON COLUMN public."GEO_DAE_declaration".xy_precis IS 'Précision des coordonnées X et Y';
COMMENT ON COLUMN public."GEO_DAE_declaration".id_adr IS 'Clé d''interopérabilité';
COMMENT ON COLUMN public."GEO_DAE_declaration".adr_num IS 'Numéro de la voie et, le cas échéant, sufixe, d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".adr_voie IS 'Type et nom de la voie ou lieu-dit d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".com_cp IS 'Code postal de la commune d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".com_insee IS 'Code Insee de la commune d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".com_nom IS 'Nom de la commune d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc IS 'Environnement d''accès du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc_lib IS 'Accès libre du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc_pcsec IS 'Présence d''un poste de sécurité';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc_acc IS 'Présence d''un accueil public';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc_etg IS 'Etage d''accessibilité du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".acc_complt IS 'Complément d''information sur l''accès au DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".photo1 IS 'Photo 1 du DAE dans son environnement';
COMMENT ON COLUMN public."GEO_DAE_declaration".photo2 IS 'Photo 2 du DAE dans son environnement';
COMMENT ON COLUMN public."GEO_DAE_declaration".disp_j IS 'Jours d''accessibilité de l''appareil';
COMMENT ON COLUMN public."GEO_DAE_declaration".disp_h IS 'Heures d''accessibilité de l''appareil';
COMMENT ON COLUMN public."GEO_DAE_declaration".disp_complt IS 'Complément d''information sur la disponibilité du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".tel1 IS 'Numéro de téléphone 1 sur le site d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".tel2 IS 'Numéro 2 de téléphone sur le site d''implantation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".site_email IS 'Adresse email de contact du site où le DAE a été implanté';
COMMENT ON COLUMN public."GEO_DAE_declaration".date_instal IS 'Date d''installation du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".etat_fonct IS 'Etat de fonctionnement du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".fab_siren IS 'Numéro SIREN du fabricant du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".fab_rais IS 'Raison sociale du fabricant du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".mnt_siren IS 'Numéro SIREN du mainteneur du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".mnt_rais IS 'Raison sociale du mainteneur';
COMMENT ON COLUMN public."GEO_DAE_declaration".modele IS 'Nom du modèle de DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".num_serie IS 'Numéro de série du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".id_euro IS 'Identifiant unique du dispositif (IUD européen)';
COMMENT ON COLUMN public."GEO_DAE_declaration".lc_ped IS 'Présence d''électrodes pédiatriques';
COMMENT ON COLUMN public."GEO_DAE_declaration".dtpr_lcped IS 'Date de péremption des électrodes pédiatriques';
COMMENT ON COLUMN public."GEO_DAE_declaration".dtpr_lcad IS 'Date de péremption des électrodes adultes';
COMMENT ON COLUMN public."GEO_DAE_declaration".dtpr_bat IS 'Date de péremption de la batterie';
COMMENT ON COLUMN public."GEO_DAE_declaration".freq_mnt IS 'Fréquence de la maintenance';
COMMENT ON COLUMN public."GEO_DAE_declaration".dispsurv IS 'Dispositif de surveillance à distance du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".dermnt IS 'Date de la dernière maintenance du DAE';
COMMENT ON COLUMN public."GEO_DAE_declaration".maj_don IS 'Date de la dernière mise à jour des données';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_siren IS 'Numéro SIREN de l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_type IS 'Code APE de l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_rais IS 'Raison sociale de l''exploitant - personne morale';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_num IS 'Numéro de la voie et, le cas échéant, sufixe, où est situé l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_voie IS 'Type et nom de la voie ou lieu-dit où est situé l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_com IS 'Commune où est situé l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_cp IS 'Code postal où est situé l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_insee IS 'Code Insee de la commune où est situé l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_tel1 IS 'Numéro de téléphone 1 de l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_tel2 IS 'Numéro de téléphone 2 de l''exploitant';
COMMENT ON COLUMN public."GEO_DAE_declaration".expt_mail IS 'Adresse électronique de l''exploitant';
