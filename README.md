# GEO'DAE et QGIS
Je reprends ici les termes du site officiel, car je ne dirais pas mieux :
>À peine 1 citoyen sur 10 survit à un arrêt cardiaque faute d’avoir bénéficié au bon moment de l’intervention d’une personne. Cette personne aurait pu leur sauver la vie en pratiquant les gestes de premier secours et en relançant le cœur par un choc électrique (défibrillation) le temps que les secours interviennent. 40 000 à 50 000 arrêts cardiaques par an sont recensés.
>L’utilisation d’un DAE lors d’un arrêt cardiaque augmente de 40% les chances de survie.
>Le plan national de santé publique prévoit de former 80 % de la population aux gestes de premiers secours et d’améliorer l’accès aux défibrillateurs automatisés externes sur le territoire national, en favorisant leur géolocalisation et leur maintenance.
>La création d’une base de données nationale, disposition de la loi du 28 juin 2018 relative au défibrillateur cardiaque, s’inscrit dans cette ambition. Tous les exploitants de DAE doivent désormais déclarer les données de leurs défibrillateurs dans la base nationale.

**Avertissement  : les couches et méthodes décrites ne sont que des aides. Ils vous appartient, si vous voulez les utiliser de vérifier l'exactitude  et la compatibilité des informations avec les outils officiels disponibles sur le site du ministère. Je ne peux être tenu responsable d'une erreur ou d'un manque de déclaration, il vous appartient de vérifier vos informations**

- [site de GEO'DAE](https://geodae.atlasante.fr/apropos)
- [article du blog pasq.fr](https://pasq.fr/geodae-et-qgis)
